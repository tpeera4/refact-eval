# Evaluation of Refactoring

1) Require a running instance of mongodb to persist analysis results (e.g. a mongodb docker instance)
* It is also helpful to install Robo 3T, a GUI interface to view data stored in MongoDB
https://robomongo.org/download 

2) Require a running server `tidyblocks` when downloading XML source file of a project

3) For this project to work properly with Eclipse, lombok plugin needs to be installed
 <https://projectlombok.org/> 
 
4) Set XML source directory path to your preferred local directory in `refact/eval/ProjectConstantConfig.java` 

5). install <https://marketplace.eclipse.org/content/ansi-escape-console> to properly render logs in console.
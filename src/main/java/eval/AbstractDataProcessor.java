package eval;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.bson.Document;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.mongodb.client.FindIterable;

import data.ProjectSourceLocalRepo;
import data.db.ProjectMongoDataset;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractDataProcessor {
	@Setter
	public int processingTimeOutInSeconds = 3;
	protected final ProjectSourceLocalRepo projectXmlRepo;
	protected final AtomicInteger counter;
	protected final ScheduledExecutorService executor;
	protected final Stream<String> projectIdStream;
	protected final ProjectMongoDataset inputDataset;
	Marker stageInfoMarker = MarkerFactory.getMarker("stageinfo");

	public AbstractDataProcessor(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo) {
		this.inputDataset = inputDataset;
		this.counter = new AtomicInteger(0);
		this.executor = Executors.newScheduledThreadPool(1);// Runtime.getRuntime().availableProcessors() - 1
		this.projectXmlRepo = projectXmlRepo;
		this.projectIdStream = getProjectIdStream();
	}

	public void run() {
		List<Callable<String>> projectIdCallables = generateCallableList();
		projectIdCallables.stream().map(callable -> executor.submit(callable)).map(future -> {
			executor.schedule(new Runnable() {
				public void run() {
					future.cancel(true);
				}
			}, processingTimeOutInSeconds, TimeUnit.SECONDS);

			if (future.isCancelled()) {
				return "timeout";
			}
			String projectId;
			try {
				projectId = future.get();
				showProgress(projectId + "- success");
				return projectId;
			} catch (Exception e) {
				showProgress(e.getMessage());
				throw new IllegalStateException(e);
			}

		}).count();

		executor.shutdown();
	}

	public void showProgress(String projectId) {
		log.trace(stageInfoMarker, "processed {}", projectId);
	}

	public List<Callable<String>> generateCallableList() {
		return projectIdStream.map(projectId -> (Callable<String>) (() -> {
			log.trace(stageInfoMarker, "processing {}", projectId);
			try {
				processProject(projectId);
			} catch (Exception e) {
				log.error("fail to process {}", projectId);
			}
			return projectId;
		})).collect(Collectors.toList());
	}

	public abstract void processProject(String projectId);

	public Stream<String> getProjectIdStream() {
		if (inputDataset == null) {
			log.error("Need to provide projectId stream or ProjectMongoDataset inputDataset");
			return Stream.empty();
		}
		return extractProjectIdStream(inputDataset);
	}

	public static Stream<String> extractProjectIdStream(ProjectMongoDataset inputDataset) {
		FindIterable<Document> datasetIterable = inputDataset.getAll();
		Iterable<Document> projectIterable = () -> datasetIterable.iterator();
		Stream<Document> docStream = StreamSupport.stream(projectIterable.spliterator(), true);
		Stream<String> projectIdStream = docStream.map(doc -> doc.getString("_id"));
		return projectIdStream;
	}
}

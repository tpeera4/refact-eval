package eval.dataprep;

import java.util.List;

import com.google.common.collect.Lists;

import data.ProjectSourceLocalRepo;
import data.db.ProjectMongoDataset;
import eval.AbstractDataProcessor;
import eval.Configurations;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectXmlProcessor extends AbstractDataProcessor {
	@Getter
	List<String> collectedProjectIds = Lists.newArrayList();

	public ProjectXmlProcessor(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo)
			throws Exception {
		super(inputDataset, projectXmlRepo);
	}

	@Override
	public void showProgress(String projectId) {
		log.info("{} - Processed:{}, {}/{}", counter.incrementAndGet(), projectId, projectXmlRepo.size(),
				inputDataset.size());
	}

	@Override
	public void processProject(String projectId) {
		if (projectXmlRepo.contains(projectId)) {
			collectedProjectIds.add(projectId);
			return;
		}

		String url = String.format("http://localhost:8080/projects/%s/xml", projectId);
		try {
			String xmlStr = Utility.httpGetRequest(url);
			projectXmlRepo.save(projectId, xmlStr);
			collectedProjectIds.add(projectId);
		} catch (Exception e) {
			log.error("Failure XML extraction for {}", projectId);
		}
	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset("iseud19", "all_projects-2");
		ProjectXmlProcessor processor = new ProjectXmlProcessor(inputDataset, xmlDataset);
		processor.setProcessingTimeOutInSeconds(60);
		processor.run();
		log.info("Collected {} projects", processor.getCollectedProjectIds().size());

	}

}

package eval.dataprep;

import org.bson.Document;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.client.MongoCursor;

import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ModifyProjectInfoProcessor {
	public static void main(String[] args) throws ProjectDatabaseException {
		ProjectMongoDataset dataset = new ProjectMongoDataset("iseud19", "all-projects-2");
		MongoCursor<Document> itr = dataset.getAll().iterator();
		try {
			while (itr.hasNext()) {
				Document doc = itr.next();
				String projectId = doc.getString("_id");
				String urlToRead = "https://api.scratch.mit.edu/projects/" + projectId;
				String resp = Utility.httpGetRequest(urlToRead);
				JsonObject projectInfo = new JsonParser().parse(resp).getAsJsonObject();
				int remixCount = projectInfo.get("stats").getAsJsonObject().get("remixes").getAsInt();
				doc.put("statRemixes", remixCount);
				dataset.put(doc);
				log.info("Updated {}", projectId);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
package eval.dataprep;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import data.model.ProjectMetadata;

public class ScratchProjectJsonArrayReader {
	JsonArray record;
	
	public ScratchProjectJsonArrayReader(File file) throws IOException {
		JsonParser parser = new JsonParser();
		String string = FileUtils.readFileToString(file);
		record = parser.parse(string).getAsJsonArray();
	}
	public int getSize() {
		return record.size();
	}
	public JsonObject getProjectRaw(int i) {
		return record.get(i).getAsJsonObject();
	}
	public ProjectMetadata getProject(int i) {
		return new ProjectMetadata(getProjectRaw(i));
	}
}

package eval.routine;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.naming.TimeLimitExceededException;

import org.bson.Document;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import anlys.refact.ExtractConst;
import anlys.refact.ExtractProcedure;
import anlys.refact.MultiPassRefactorer;
import anlys.smell.DuplicateConstAnalyzer;
import anlys.smell.DuplicateConstant;
import anlys.smell.SmellAnalyzer;
import ast.AssignStmt;
import ast.BlockSeq;
import ast.BroadcastStmt;
import ast.Expr;
import ast.IfElseStmt;
import ast.IncrementStmt;
import ast.Literal;
import ast.LoopStmt;
import ast.Program;
import ast.ReceiveStmt;
import ast.ScratchBlock;
import ast.Script;
import ast.Stmt;
import ast.StrLiteral;
import data.ProjectSourceLocalRepo;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import eval.RefactEvaluator;
import eval.Configurations;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProjectFileWriter;

@Slf4j
public class PlaygroundEvaluator extends RefactEvaluator {
	public static final String COLLECTION_PREFIX = "pg";
	@Getter
	List<String> parsableProjects = Lists.newArrayList();

	public PlaygroundEvaluator(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo)
			throws ProjectDatabaseException {
		super(inputDataset, projectXmlRepo, COLLECTION_PREFIX);
	}

	@Override
	public void processProject(String projectId) {
		try {
			Program program = retrieveXmlAndParse(projectId);
			parsableProjects.add(projectId);
			// duplicateSprite(program, projectId);
			// printNestedIf(program, projectId);
			// longStringLiteral(program, projectId);
			// hasIndexedLoop(program, projectId);
			// hasMiddleMan(program, projectId);
			// broadcastToOneReceiverInSameSprite(program, projectId);
		} catch (IOException e) {
			log.error("Unable to get Xml file for {}", projectId);
		} catch (TimeLimitExceededException e) {
			log.error("{} exceeded time limit!", projectId);
		} catch (Exception e) {
			log.error("fail to process {}\n{}", projectId, e.getLocalizedMessage());

		}
		log.info("...........................", projectId);

	}

	private void duplicateSprite(Program program, String projectId) {
		// sprite with same number of event script (ignore unreachable)

	}

	private void printNestedIf(Program program, String projectId) {
		List<IfElseStmt> allIfStmts = program.blockColl().stream().filter(b -> b.getOpcode().equals("control_if"))
				.map(b -> (IfElseStmt) b).collect(Collectors.toList());
		List<IfElseStmt> nestedIfStmts = allIfStmts.stream().filter(
				iff -> iff.getThen().blockColl().stream().filter(b -> b.getOpcode().equals("control_if")).count() > 0)
				.collect(Collectors.toList());
		log.info(projectId);
		nestedIfStmts.forEach(iff -> log.info("{}\n{}", iff.sprite().getName(), iff.toDevString()));
	}

	private void longStringLiteral(Program program, String projectId) {
		List<StrLiteral> literalList = program.exprColl().stream().filter(e -> e instanceof StrLiteral)
				.map(l -> (StrLiteral) l).collect(Collectors.toList());
		List<StrLiteral> longLits = literalList.stream().filter(l -> l.getValue().length() > 5)
				.collect(Collectors.toList());
		if (!longLits.isEmpty()) {
			log.info(projectId);
			longLits.forEach(
					l -> log.info("{} : {}/{}", l.getValue(), l.sprite().getName(), l.parentStmt()));
		}
	}

	private void broadcastToOneReceiverInSameSprite(Program program, String projectId) {
		if (!program.broadcasters().isEmpty()) {
			BroadcastStmt b = program.broadcasters().iterator().next();
			HashSet<ReceiveStmt> rSet = program.getReceiverOf(b);
			ReceiveStmt receiverScript = rSet.iterator().next();
			if (rSet.size() == 1 && b.sprite() == receiverScript.sprite()) {
				log.info(projectId);
				log.info(b.sprite().getName());
				log.info("{} -> {}", b.toDevString(), receiverScript.toDevString());
				log.info("{}", receiverScript.script().toDevString());
			}
		}
	}

	private void hasMiddleMan(Program program, String projectId) {
		List<Script> whenReceiveScripts = program.coll_scripts().stream().map(s -> s.getBody().getStmt(0))
				.map(stmt -> (ScratchBlock) stmt)
				.filter(b -> b.getOpcode().equals("event_whenbroadcastreceived")).map(b -> (Stmt) b)
				.map(stmt -> stmt.script()).collect(Collectors.toList());

		if (!whenReceiveScripts.isEmpty()) {
			whenReceiveScripts.stream().filter(s -> hasBroadcast(s)).forEach(s -> {
				log.info(s.toDevString());
			});
		}

	}

	private boolean hasBroadcast(Script s) {
		return s.blockColl().stream().filter(b -> b.getOpcode().equals("event_broadcast")).count() > 0;
	}

	private void hasIndexedLoop(Program program, String projectId) {
		List<BlockSeq> loopBodyList = program.blockColl().stream()
				.filter(b -> b.getOpcode().equals("control_repeat_until"))
				.map(b -> (LoopStmt) b).map(loop -> loop.getBody()).collect(Collectors.toList());

		log.info(projectId);
		if (!loopBodyList.isEmpty()) {
			List<BlockSeq> indexedLoops = loopBodyList.stream().filter(body -> !containsVarIncrement(body).isEmpty())
					.collect(Collectors.toList());
			if (!loopBodyList.isEmpty()) {
				log.info("Found indexed loop!");

				indexedLoops.forEach(loop -> log.info("{}: {}", loop.sprite().getName(),
						loop.parentStmt().toDevString()));
			}
		}
	}

	private List<ScratchBlock> containsVarIncrement(BlockSeq body) {
		return body.blockColl().stream().filter(b -> b.getOpcode().equals("data_changevariableby"))
				.collect(Collectors.toList());
	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset(Constants.DB_NAME, "trending-projects-2");
		PlaygroundEvaluator processor = new PlaygroundEvaluator(inputDataset, xmlDataset);
		processor.run();

		log.info("Parsable {} projects", processor.getParsableProjects().size());
	}

}

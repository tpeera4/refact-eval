package eval.routine;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import anlys.metrics.MetricsKeyValue;
import anlys.metrics.ProgramMetricsAnalyzer;
import ast.Program;
import data.ProjectSourceLocalRepo;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import eval.RefactEvaluator;
import eval.AbstractDataProcessor;
import eval.Configurations;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;

@Slf4j
public class GeneralMetricsProcessor extends AbstractDataProcessor {
	
	ProjectMongoDataset outputDataset;
	public GeneralMetricsProcessor(ProjectMongoDataset inputDataset,
			ProjectSourceLocalRepo xmlDataset) throws ProjectDatabaseException {
		super(inputDataset, xmlDataset);
		outputDataset = new ProjectMongoDataset("iseud19", "parsable-projects");
	}

	@Override
	public void processProject(String projectId) {
		String projectSrc;
		try {
			projectSrc = projectXmlRepo.get(projectId);
		} catch (IOException e) {
			log.error("skip {} -- xml does not exist", projectId);
			return;
		}
		Program program = new Sb3XmlParser().parseProgram(projectSrc);
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		try {
			String jsonResult = new ObjectMapper().writeValueAsString(metrics.asMap());
			outputDataset.put(projectId, jsonResult);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset("iseud19", "all-projects-2");
		
		GeneralMetricsProcessor processor = new GeneralMetricsProcessor(inputDataset, xmlDataset);
		processor.run();
	}

}

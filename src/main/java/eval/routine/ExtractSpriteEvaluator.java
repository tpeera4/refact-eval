package eval.routine;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.naming.TimeLimitExceededException;

import org.bson.Document;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.collect.Sets;

import anlys.refact.ExtractParentSprite;
import anlys.refact.MultiPassRefactorer;
import anlys.smell.DuplicateSpriteAnalyzer;
import ast.Program;
import data.ProjectSourceLocalRepo;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import eval.Configurations;
import eval.RefactEvaluator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExtractSpriteEvaluator extends RefactEvaluator {
	public static final String COLLECTION_PREFIX = "ds";

	public ExtractSpriteEvaluator(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo)
			throws ProjectDatabaseException {
		super(inputDataset, projectXmlRepo, COLLECTION_PREFIX);
		clearCollections();
	}

	@Override
	public void processProject(String projectId) {
		Set<String> shouldSkip = Sets.newHashSet();

		if (shouldSkip.contains(projectId)) {
			return;
		}
		if (metricsAfterDS.get(projectId) != null) {
			log.info("already processed {}", projectId);
			return;
		}

		try {
			log.info("processing {} ", projectId);
			Program program = retrieveXmlAndParse(projectId);

			// pre-analysis program info
			String devStringBefore = program.toDevString();
			Document metricsBeforeDoc = computeQualityMetrics(program, projectId);
			metricsBeforeDS.put(metricsBeforeDoc);

			// smell analyzer
			DuplicateSpriteAnalyzer smellAnalyzer = new DuplicateSpriteAnalyzer();

			// refactorer
			ExtractParentSprite refactAnalyzer = new ExtractParentSprite();
			refactAnalyzer.setSmellMetricsRecorder(smellMetricsDS);
			refactAnalyzer.setRefactRecorder(refactMetricsDS);

			// multipass refactoring
			Map<String, Object> runMetrics = new MultiPassRefactorer(program, projectId).run(smellAnalyzer,
					refactAnalyzer);

			boolean programChanged = (int) runMetrics.get("changed") > 0;
			if (programChanged) {
				// post-analysis program metrics
				log.trace("compute post-analysis metrics");
				String devStringAfter = program.toDevString();
				Document metricsAfterDoc = computeQualityMetrics(program, projectId);
				metricsAfterDS.put(metricsAfterDoc);

				changeSummary(metricsBeforeDoc, metricsAfterDoc);
				generateDiff(projectId, devStringBefore, devStringAfter);
			}

		} catch (IOException e) {
			log.error("Unable to get Xml file for {}", projectId);
		} catch (TimeLimitExceededException e) {
			log.error("{} exceeded time limit!", projectId);
		} catch (Exception e) {
			log.error("fail to process {}\n{}", projectId, e);
			if(e.getLocalizedMessage()==null) {
				e.printStackTrace();
			}
		}
		log.info("...........................", projectId);

	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset(Constants.DB_NAME, "all-projects-2");
		ExtractSpriteEvaluator processor = new ExtractSpriteEvaluator(inputDataset, xmlDataset);
		processor.setProcessingTimeOutInSeconds(5);
		processor.run();
	}

}

package eval.routine;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.naming.TimeLimitExceededException;

import org.bson.Document;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.collect.Sets;

import anlys.refact.ExtractConst;
import anlys.refact.ExtractProcedure;
import anlys.refact.MultiPassRefactorer;
import anlys.refact.RescopeVar;
import anlys.smell.BroadScopeVarAnalyzer;
import anlys.smell.DuplicateConstAnalyzer;
import anlys.smell.DuplicateConstant;
import ast.Program;
import data.ProjectSourceLocalRepo;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import eval.RefactEvaluator;
import eval.Configurations;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProjectFileWriter;

@Slf4j
public class ReduceVarScopeEvaluator extends RefactEvaluator {
	public static final String COLLECTION_PREFIX = "bv";

	public ReduceVarScopeEvaluator(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo)
			throws ProjectDatabaseException {
		super(inputDataset, projectXmlRepo, COLLECTION_PREFIX);
		clearCollections();
	}

	@Override
	public void processProject(String projectId) {
		Set<String> shouldSkip = Sets.newHashSet();
		if (shouldSkip.contains(projectId)) {
			return;
		}
		if (metricsAfterDS.get(projectId) != null) {
			log.info("already processed {}", projectId);
			return;
		}

		try {
			log.info("processing {} ", projectId);
			Program program = retrieveXmlAndParse(projectId);

			// pre-analysis program info
			String devStringBefore = program.toDevString();
			Document metricsBeforeDoc = computeQualityMetrics(program, projectId);
			metricsBeforeDS.put(metricsBeforeDoc);

			// smell analyzer
			BroadScopeVarAnalyzer smellAnalyzer = new BroadScopeVarAnalyzer();

			// refactorer
			RescopeVar refactAnalyzer = new RescopeVar();
			refactAnalyzer.setSmellMetricsRecorder(smellMetricsDS);
			refactAnalyzer.setRefactRecorder(refactMetricsDS);

			// multipass refactoring
			Map<String, Object> runMetrics = new MultiPassRefactorer(program, projectId).run(smellAnalyzer,
					refactAnalyzer);

			boolean programChanged = (int) runMetrics.get("changed") > 0;
			if (programChanged) {
				// post-analysis program metrics
				log.trace("compute post-analysis metrics");
				String devStringAfter = program.toDevString();
				Document metricsAfterDoc = computeQualityMetrics(program, projectId);
				metricsAfterDS.put(metricsAfterDoc);

				changeSummary(metricsBeforeDoc, metricsAfterDoc);
				generateDiff(projectId, devStringBefore, devStringAfter);
			}

		} catch (IOException e) {
			log.error("Unable to get Xml file for {}", projectId);
		} catch (TimeLimitExceededException e) {
			log.error("{} exceeded time limit!", projectId);
		} catch (Exception e) {
			log.error("fail to process {} -- {}", projectId, e.getLocalizedMessage());
			if(e.getLocalizedMessage()==null) {
				e.printStackTrace();
			}
		}
		log.trace("Done processing {}\n...........................", projectId);

	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset(Constants.DB_NAME, "all-projects-2");
		ReduceVarScopeEvaluator processor = new ReduceVarScopeEvaluator(inputDataset, xmlDataset);
		processor.setProcessingTimeOutInSeconds(5);
		processor.run();
	}

}

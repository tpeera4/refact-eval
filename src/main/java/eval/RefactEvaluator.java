package eval;

import java.text.NumberFormat;
import java.text.ParseException;

import org.bson.Document;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import anlys.metrics.Metric;
import anlys.metrics.MetricKey;
import anlys.metrics.MetricsKeyValue;
import anlys.metrics.ProgramMetricsAnalyzer;
import ast.Program;
import data.ProjectSourceLocalRepo;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import data.model.ProjectIdBasedResult;
import eval.routine.Constants;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProjectFileWriter;

@Slf4j
public abstract class RefactEvaluator extends AbstractDataProcessor {
	protected ProjectMongoDataset metricsBeforeDS;
	protected ProjectMongoDataset metricsAfterDS;
	protected ProjectMongoDataset smellMetricsDS;
	protected ProjectMongoDataset refactMetricsDS;
	protected DiffTextGenerator diffTextGenerator;
	protected ProjectFileWriter projectFileWriter;

	public RefactEvaluator(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo,
			String collPrefix) throws ProjectDatabaseException {
		super(inputDataset, projectXmlRepo);

		projectFileWriter = new ProjectFileWriter();
		projectFileWriter.appendBasePath(collPrefix);
		diffTextGenerator = new DiffTextGenerator(projectFileWriter);

		metricsBeforeDS = new ProjectMongoDataset(Constants.DB_NAME, formatCollName(collPrefix, "metrics_before"));
		metricsAfterDS = new ProjectMongoDataset(Constants.DB_NAME, formatCollName(collPrefix, "metrics_after"));
		smellMetricsDS = new ProjectMongoDataset(Constants.DB_NAME, formatCollName(collPrefix, "smell"));
		refactMetricsDS = new ProjectMongoDataset(Constants.DB_NAME, formatCollName(collPrefix, "refact"));

	}

	protected void clearCollections() {
		// clear collections
		metricsBeforeDS.drop();
		metricsAfterDS.drop();
		smellMetricsDS.drop();
		refactMetricsDS.drop();
	}

	public Program retrieveXmlAndParse(String projectId) throws Exception {
		String xmlStr;
		xmlStr = projectXmlRepo.get(projectId);

		Program program;
		try {
			program = new Sb3XmlParser().parseProgram(xmlStr);
		} catch (Exception e) {
			throw new Exception("Parsing Program Exception");
		}
		log.trace("parsed program xml");
		return program;
	}

	public static String formatCollName(String collectionPrefix, String name) {
		return collectionPrefix + "_" + name;
	}

	public static Document computeQualityMetrics(Program program, String projectId) throws JsonProcessingException {
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		ProjectIdBasedResult result = new ProjectIdBasedResult(projectId)
				.append(Metric.Size.num_blocks, metrics.get(Metric.Size.num_blocks))
				.append(Metric.Size.locs, metrics.get(Metric.Size.locs))
				.append(Metric.Size.num_scriptables, metrics.get(Metric.Size.num_scriptables))
				.append(Metric.Entity.procedure_dens, metrics.get(Metric.Entity.procedure_dens))
				.append(Metric.Quality.long_script_dens, metrics.get(Metric.Quality.long_script_dens))
				.append(Metric.Quality.complex_script_dens,
						metrics.get(Metric.Quality.complex_script_dens))
				.append(Metric.Entity.num_global_var, metrics.get(Metric.Entity.num_global_var))
				.append(Metric.Entity.num_local_var, metrics.get(Metric.Entity.num_local_var))
				.append(Metric.Entity.num_literals, metrics.get(Metric.Entity.num_literals))
				.append(Metric.Entity.num_create_clone_of, metrics.get(Metric.Entity.num_create_clone_of));

		return result.toDoc();
	}

	public void generateDiff(String projectId, String devStringBefore, String after) throws Exception {
		diffTextGenerator.compare(devStringBefore, after).toPrettyHtml(projectId);
	}

	public static void changeSummary(Document metricsBeforeDoc, Document metricsAfterDoc) {
		log.info("Before Metrics:{}", metricsBeforeDoc);
		log.info("After Metrics:{}", metricsAfterDoc);

		for (String k : metricsAfterDoc.keySet()) {
			if (k.equals("_id")) {
				continue;
			}
			Object after = metricsAfterDoc.get(k);
			Object before = metricsBeforeDoc.get(k);

			try {
				Number valAfter = NumberFormat.getInstance().parse(after.toString());
				Number valBefore = NumberFormat.getInstance().parse(before.toString());
				double difference = valAfter.doubleValue() - valBefore.doubleValue();
				String diffStr = String.format("%.2f", difference);
				StringBuilder percent = new StringBuilder();
				if (valBefore.doubleValue() != 0) {
					percent.append("(").append(String.format("%.2f", difference / valBefore.doubleValue() * 100))
							.append(")%");
				}
				if (difference > 0) {
					diffStr = "+" + diffStr;
				}
				log.info("\t* {}:\t{} {}", k, diffStr, percent);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

	}

}

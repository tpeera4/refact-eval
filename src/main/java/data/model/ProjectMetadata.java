package data.model;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import eval.dataprep.Utility;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Setter
@Getter
@Slf4j
public class ProjectMetadata {
	@JsonProperty("_id")
	String projectId;
	String author;
	int statViews;
	int statRemixes;

	public ProjectMetadata(JsonObject projectRaw) {
		projectId = projectRaw.get("id").getAsString();
		author = projectRaw.get("author").getAsJsonObject().get("username").getAsString();
		statViews = projectRaw.get("stats").getAsJsonObject().get("views").getAsInt();
		statRemixes = projectRaw.get("stats").getAsJsonObject().get("remixes").getAsInt();
	}

	public String toJsonStr() throws JsonProcessingException {
		ObjectMapper jsonMapper = new ObjectMapper();
		return jsonMapper.writeValueAsString(this);
	}

}

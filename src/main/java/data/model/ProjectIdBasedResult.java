package data.model;

import java.util.Map;

import org.bson.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import anlys.metrics.MetricKey;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ProjectIdBasedResult implements Result {
	String projectId;
	Map<String, Object> map = Maps.newHashMap();

	public ProjectIdBasedResult(String projectId) {
		this.map.put("_id", projectId);
	}

	public ProjectIdBasedResult(String resultId, String projectId) {
		if (resultId == null || projectId == null) {
			throw new IllegalArgumentException("result id  and project id must not be null");
		}
		this.map.put("_id", resultId);
		this.map.put("projectId", projectId);
	}

//	public ProjectIdBasedResult append(MetricKey key, Object value) {
//		return append(key.name(), value);
//	}

	public ProjectIdBasedResult append(String key, Object value) {
		if (key.equals("projectId")) {
			throw new IllegalArgumentException("projectId can be set only once");
		}
		this.map.put(key, value);
		return this;
	}

	@Override
	public Document toDoc() throws JsonProcessingException {
		String jsonStr = new ObjectMapper().writeValueAsString(this.map);
		return Document.parse(jsonStr);
	}

	public ProjectIdBasedResult append(MetricKey key, Object value) {
		append(key.name().toLowerCase(), value);
		return this;
	}
}

package data.model;

import org.bson.Document;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface Result {

	Document toDoc() throws JsonProcessingException;

}

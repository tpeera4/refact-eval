package data;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import util.ProjectFileWriter;

@Slf4j
@Getter
@Setter
public class ProjectSourceLocalRepo {
	String basePath;

	public ProjectSourceLocalRepo(String pathToXmlSourceFileDir) {
		basePath = pathToXmlSourceFileDir;
	}

	/**
	 * 
	 * @param projectId
	 * @return XML representation of the project
	 * @throws IOException
	 */
	public String get(String projectId) throws IOException {
		String xmlFilePath = basePath + projectId + ".xml";
		return FileUtils.readFileToString(new File(xmlFilePath));
	}

	public boolean save(String projectId, String data) throws IOException {
		new ProjectFileWriter(basePath).write(data, projectId, "xml");
		return true;
	}

	public boolean contains(String projectId) {
		File directory = new File(basePath);
		String xmlFilePath = basePath + projectId + ".xml";
		boolean contained = false;
		try {
			contained = FileUtils.directoryContains(directory, new File(xmlFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contained;
	}

	public int size() {
		return new File(basePath).listFiles().length;
	}
}

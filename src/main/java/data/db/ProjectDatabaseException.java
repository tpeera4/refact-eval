package data.db;

public class ProjectDatabaseException extends Exception {

	public ProjectDatabaseException(String msg) {
		super(msg);
	}

}

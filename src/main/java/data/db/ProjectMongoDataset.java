package data.db;

import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;

import anlys.Recordable;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectMongoDataset extends MongoDataset implements Recordable {

	public ProjectMongoDataset(String databaseName, String collectionName) throws ProjectDatabaseException {
		super(databaseName, collectionName);
	}

	public void put(String jsonString) {
		Document doc = Document.parse(jsonString);
		if (!doc.containsKey("_id")) {
			log.warn("Field _id should required to properly store the result (normally _id corresponds to projectId)");
		}
		put(doc);
	}

	public void put(String id, String jsonString) {
		Document doc = Document.parse(jsonString);
		doc.putIfAbsent("_id", id);
		put(doc);
	}

	/**
	 * upsert
	 * 
	 * @param doc
	 */
	public void put(Document doc) {
		if (get(doc.getString("_id")) != null) {
			log.warn("{} - document id: {} already exists", collectionName, doc.getString("_id"));
		}

		UpdateOptions options = new UpdateOptions().upsert(true);
		Bson filter = Filters.eq("_id", doc.getString("_id"));
		collection.replaceOne(filter, doc, options);
	}

	public long size() {
		return collection.count();
	}

	public void drop() {
		collection.drop();
	}

	public Document get(String id) {
		Bson filter = Filters.eq("_id", id);
		FindIterable<Document> results = collection.find(filter);
		return results.first();
	}

	public FindIterable<Document> getAll() {
		return collection.find();
	}

	@Override
	public boolean record(Map<String, Object> record) {
		String jsonStr;
		try {
			jsonStr = new ObjectMapper().writeValueAsString(record);
			put(Document.parse(jsonStr));
			return true;
		} catch (JsonProcessingException e) {
			return false;
		}

	}
}

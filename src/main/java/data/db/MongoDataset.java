package data.db;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MongoDataset {
	@Getter
	String collectionName;
	protected MongoCollection<Document> collection;

	public MongoDataset(String databaseName, String collectionName) throws ProjectDatabaseException {
		collection = new DBConnection(databaseName, collectionName).getCollection();
	}
}

package data.db;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientOptions.Builder;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class DBConnection {
	String dbName;
	String colName;
	private MongoDatabase database;
	private static MongoClient mongoClient = null;

	public DBConnection(String databaseName, String collectionName) throws ProjectDatabaseException {
		this.dbName = databaseName;
		this.colName = collectionName;
		try {
			createNewMongoDBClient();
		} catch (Exception e) {
			mongoClient.close();
			throw new ProjectDatabaseException("Mongo server is down");
		}
		database = mongoClient.getDatabase(dbName);

	}

	private static void createNewMongoDBClient() {
		if (mongoClient == null) {
			log.trace("...connecting to mongo server...");
			mongoClient = new MongoClient("localhost", 27017);
			mongoClient.getAddress();
			log.trace("...connected...");
		}
	}

	public MongoCollection<Document> getCollection() {
		return database.getCollection(colName);

	}
}

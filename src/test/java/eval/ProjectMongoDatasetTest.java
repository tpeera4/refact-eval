package eval;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.bson.Document;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;

import anlys.AnalysisResult;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import data.model.ProjectMetadata;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectMongoDatasetTest {

	@Test
	public void test() throws ProjectDatabaseException, JsonProcessingException {
		ProjectMongoDataset resultColl = new ProjectMongoDataset("vlhcc19", "test");
		resultColl.drop();

		AnalysisResult result = new AnalysisResult();
		result.setMetadata("_id", "abc");
		result.setMetadata("num", 5);
		result.setMetadata("num2", 5.4);
		result.setMetadata("arr", Lists.newArrayList("a", "b"));
		result.setMetadata("dict", ImmutableMap.of("k1", "v1", "k2", "v2"));

		String resJson = result.getMetadataAsJson();

		resultColl.put(resJson);

		AnalysisResult result2 = new AnalysisResult();
		result2.setMetadata("_id", "efg");
		resultColl.put(result2.getMetadataAsJson());

		assertThat(resultColl.size(), equalTo(2L));
		resultColl.drop();
		assertThat(resultColl.size(), equalTo(0L));

	}

	@Test
	public void shouldInsertProjectMetadata() throws ProjectDatabaseException, JsonProcessingException {
		ProjectMetadata metadata = new ProjectMetadata();
		metadata.setAuthor("author");
		metadata.setProjectId("123");
		metadata.setStatRemixes(123);
		metadata.setStatViews(2);

		ProjectMongoDataset resultColl = new ProjectMongoDataset("testdb", "test");
		resultColl.drop();
		resultColl.put(metadata.toJsonStr());
	}

	@Test
	public void shouldListDBEntries() throws ProjectDatabaseException, JsonProcessingException {
		ProjectMetadata metadata = new ProjectMetadata();
		metadata.setAuthor("author");
		metadata.setProjectId("123");
		metadata.setStatRemixes(123);
		metadata.setStatViews(2);

		ProjectMongoDataset resultColl = new ProjectMongoDataset("testdb", "test");
		resultColl.drop();
		resultColl.put(metadata.toJsonStr());
		assertThat(resultColl.get("1234"), is(nullValue()));
		assertThat(resultColl.get("123"), is(notNullValue()));
		assertThat(resultColl.get("123").getString("author"), is("author"));

		FindIterable<Document> docs = resultColl.getAll();
		assertThat(docs.iterator().hasNext(), is(true));
	}
}

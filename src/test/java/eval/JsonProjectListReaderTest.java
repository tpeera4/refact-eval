package eval;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import data.model.ProjectMetadata;
import eval.dataprep.ScratchProjectJsonArrayReader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonProjectListReaderTest {
	private ScratchProjectJsonArrayReader reader;

	@Before
	public void setup() throws IOException {
		File file = new File(getClass().getClassLoader().getResource("project-list.json").getFile());
		reader = new ScratchProjectJsonArrayReader(file);
	}

	@Test
	public void shouldReadScratchProjectListing() throws IOException {

		assertThat(reader.getSize(), is(1));
		assertThat(reader.getProjectRaw(0), is(notNullValue()));
		ProjectMetadata projectMeta = reader.getProject(0);
		assertThat(projectMeta.getProjectId(), is("227340452"));
		assertThat(projectMeta.getAuthor(), is("atomicmagicnumber"));
		assertThat(projectMeta.getStatViews(), is(38455));
		assertThat(projectMeta.getStatRemixes(), is(0));
	}

}

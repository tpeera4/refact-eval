package eval;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import anlys.metrics.MetricKey;
import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import data.model.ProjectIdBasedResult;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectIdBasedResultTest {
	public enum TestKey implements MetricKey {
		BLOCKS, COMPLEXITY, AVG_SCRIPT_LENGTH, INSTANCE_SIZE, GROUP_SIZE, NUM_PARAMETER, FAILED_PRECOND_1, FAILED_PRECOND_2
	}

	@Test
	public void testProjectIdPrimiaryKeyBasedResult() throws JsonProcessingException, ProjectDatabaseException {
		ProjectMongoDataset beforeMetricsColl = new ProjectMongoDataset("testdb", "before");
		beforeMetricsColl.drop();
		ProjectMongoDataset afterMetricsColl = new ProjectMongoDataset("testdb", "after");
		afterMetricsColl.drop();

		// test entry # 1
		beforeMetricsColl.put(new ProjectIdBasedResult("1")
				.append(TestKey.BLOCKS, 10)
				.append(TestKey.COMPLEXITY, 5)
				.append(TestKey.AVG_SCRIPT_LENGTH, 8.5).toDoc());

		afterMetricsColl.put(new ProjectIdBasedResult("1")
				.append(TestKey.BLOCKS, 6)
				.append(TestKey.COMPLEXITY, 2)
				.append(TestKey.AVG_SCRIPT_LENGTH, 5.5).toDoc());

		// test entry # 2

		beforeMetricsColl.put(new ProjectIdBasedResult("2")
				.append(TestKey.BLOCKS, 20)
				.append(TestKey.COMPLEXITY, 10)
				.append(TestKey.AVG_SCRIPT_LENGTH, 10).toDoc());

		afterMetricsColl.put(new ProjectIdBasedResult("2")
				.append(TestKey.BLOCKS, 15)
				.append(TestKey.COMPLEXITY, 8)
				.append(TestKey.AVG_SCRIPT_LENGTH, 8).toDoc());

		// test entry # 3

		beforeMetricsColl.put(new ProjectIdBasedResult("3")
				.append(TestKey.BLOCKS, 30)
				.append(TestKey.COMPLEXITY, 10)
				.append(TestKey.AVG_SCRIPT_LENGTH, 10).toDoc());

		// test intentionally left out afterMetrics result for test project id 3 (when
		// no refactoring could be applied)
	}

	@Test
	public void testResultIdAsPrimaryWithProjectIdLinkResult()
			throws ProjectDatabaseException, JsonProcessingException {
		ProjectMongoDataset smellResults = new ProjectMongoDataset("testdb", "smell-duplicate_code");
		ProjectMongoDataset refactResults = new ProjectMongoDataset("testdb", "refact-extract_procedure");
		smellResults.drop();
		refactResults.drop();
		smellResults.put(new ProjectIdBasedResult("smellId1", "1")
				.append(TestKey.INSTANCE_SIZE, 10)
				.append(TestKey.GROUP_SIZE, 4)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId1", "1")
				.append(TestKey.FAILED_PRECOND_1, true)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId2", "1")
				.append(TestKey.INSTANCE_SIZE, 5)
				.append(TestKey.GROUP_SIZE, 3)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId2", "1")
				.append(TestKey.NUM_PARAMETER, 1)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId3", "2")
				.append(TestKey.INSTANCE_SIZE, 8)
				.append(TestKey.GROUP_SIZE, 4)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId3", "2")
				.append(TestKey.FAILED_PRECOND_1, true)
				.append(TestKey.FAILED_PRECOND_2, true)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId4", "2")
				.append(TestKey.INSTANCE_SIZE, 5)
				.append(TestKey.GROUP_SIZE, 5)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId4", "2")
				.append(TestKey.NUM_PARAMETER, 3)
				.toDoc());

	}

}
